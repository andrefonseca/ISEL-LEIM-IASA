package Game.AI;

public interface Action {

    public void execute();
}
