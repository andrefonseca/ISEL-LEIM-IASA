package Game.Environment;

public enum EnvironmentEvent implements Stimulus {
    EXIT, NOISE, SILENCE, ENEMY, FLEE, VICTORY, DEFEAT;
}

